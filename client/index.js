(function() {
    var GroceryApp = angular.module("GroceryApp", ["ui.router"]);

    var GroceryConfig = function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state("main", {
				url: "/main",
				templateUrl: "/views/main.html",
				controller: "GroceryCtrl as GroceryCtrl"

			})
			.state("search", {
				url: "/search/:term",
				templateUrl: "/views/search.html",
				controller: "SearchCtrl as searchCtrl"
            })
            .state("add",{
                url: "/add/:term",
                templateUrl: "/views/add.html",
                controller: "AddCtrl as addCtrl"
            });

		$urlRouterProvider.otherwise("/main");
	}
	GroceryConfig.$inject = [ "$stateProvider", "$urlRouterProvider" ]
   

   var GrocerySvc = function ($http, $q) {
        var grocerySvc = this;

       grocerySvc.showAllProducts = function() {
            var defer = $q.defer();

           $http.get("/grocery")
                .then(function(result) {
                    defer.resolve(result.data);
                }).catch(function(err) {
                    defer.reject(err);
                });
            return(defer.promise);
        };

   };
    GrocerySvc.$inject = ["$http", "$q"];

   var GroceryCtrl = function(GrocerySvc) {
        var groceryCtrl = this;
        
       GrocerySvc.showAllProducts()
            .then(function(result) {
                console.info(">>> groceries shown!", result);
                groceryCtrl.groceries = result;
            }).catch(function(err) {
                console.error("error = ", err);
            })

   }

   GrocerySvc.searchByBrand = function(searchTerm) {
    var defer = $q.defer();
    $http.get("/api/grocery" + searchTerm)
        .then(function(result) {
            console.log(result);
            defer.resolve(result.data);
        }).catch(function(err) {
            defer.reject(err);
        });
        return(defer.promise);
};

GroceryCtrl.getSearchResults = function() {
    console.log(">>>>Search for %s", groceryCtrl.searchTerm);
    GrocerySvc.searchByBrand(groceryCtrl.searchTerm)
        .then(function(result) {
            console.info(">> search results... ", result);
            groceryCtrl.groceries = result;
            groceryCtrl.notfound = false;      
        }).catch(function(err) {
            console.error("search error = ", err);
            groceryCtrl.notfound = true;
            
        })
    }
    var AddCtrl = function($state) {
        var addCtrl = this;
        addCtrl.add = function() {
            $state.go("add");
        }
    }

    AddCtrl.$inject = [ "$state" ];

    GroceryApp.config(GroceryConfig);
    GroceryApp.service("GrocerySvc", GrocerySvc);
    GroceryApp.controller("GroceryCtrl", GroceryCtrl);
    GroceryApp.controller("AddCtrl", AddCtrl);

})();