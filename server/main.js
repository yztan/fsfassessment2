//Load the libs
const q = require("q");
const path = require("path");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const express = require("express");

const pool = mysql.createPool({
    host: "localhost", port: 3306,
    user: "root", password: "password",
    database: "groceries",
    connectionLimit: 4
});

const mkQuery = function(sql, pool) {
    return (function(){
        const defer = q.defer();

        const args = [];
        for (var i in arguments)
            args.push(arguments[i]);

        pool.getConnection(function(err, conn){
            if(err) {
                defer.reject(err);
                return;
            }

            conn.query(sql, args, function(err, result){
                if(err)
                    defer.reject(err);
                else
                    defer.resolve(result);
                conn.release();
            });
        });

        return (defer.promise);
    });
}



//Create an instance of the application
app = express();
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

const SHOW_ALL_PRODUCTS = "SELECT * from GROCERY_LIST ORDER BY ID LIMIT 20";

const showAllProducts = mkQuery(SHOW_ALL_PRODUCTS, pool);

const SELECT_PRODUCT_BY_NAME= "select upc12, brand, name from grocery_list where brand like ? or name like ? limit 20";

const findProductByName = mkQuery(SELECT_PRODUCT_BY_NAME, pool);
const SQL_INSERT_PRODUCT = 
" insert into registrations(BRAND, NAME, UPC12) values(?, ?, ?)";

app.get("/grocery", function(req,resp) {
    showAllProducts()
        .then(function(result) {
            resp.status(200);
            resp.type("application/json");
            resp.json(result);
        }).catch(function(err) {
            resp.status(500);
            resp.type("application/json");
            resp.json(err);
        });
});

app.get("/register", function(req, resp) {
    
        pool.getConnection(function(err, conn) {
            //Connection pool error
            if (err) {
                console.error(">>>> error: ", err);
                resp.status(500);
                resp.end(JSON.stringify(err));
                return;
            }
            //We have a connection
            conn.query(SQL_INSERT_PRODUCT
                , [ req.query.brand, req.query.name, req.query.upc12 ]
                , function(err, result) {
                    if (err) {
                        console.error(">>>> insert: ", err);
                        resp.status(500);
                        resp.end(JSON.stringify(err));
                        conn.release();
                        return;
                    }
                });
            
            conn.release();
            resp.status(200);
            resp.type("text/html");
            resp.send("<h2>Added</h2>");
        })
    
    
    });
    

app.get("/api/grocery/:term", function(req,resp) {
    const searchTerm = "%" + req.params.term + "%"
    console.log("search term = %s", searchTerm);
    searchByBrand(searchTerm)
        .then(function(result) {
            if (result.length>0) {
                console.log(result)
                resp.status(200);
                resp.type("application/json");
                resp.json(result);
            } else {
                resp.status(404);
                resp.type("application/json");
                resp.end();
            }
        });
});
app.get("/getInfo/:term", function(req,resp){
	const searchTerm = "%" + req.params.term + "%";
	var searchResult = "";
	console.log (">>> In server app. get :search >>> %s", searchTerm);
	//Get a connection from the pool
    //Error or a connection object
    pool.getConnection(function(err, conn) {
        //Check if the pool has returned an error
        if (err) {
            console.error("Error: ", err);
            handleError(err, resp);
            return;
		}
		conn.query(SELECT_GROCERYLIST_SEARCH, [searchTerm, searchTerm], 
				function(err, result){
			//Check if the query has resulted in an error
            if (err) {
                console.error(">>>> select: ", err);
                resp.status(500);
                resp.end(JSON.stringify(err));

                conn.release();
                return;
			}
			searchResult = result;
			//console.log(">>>>Search result >>>>");
			//console.log(searchResult);
			resp.status(200);
			resp.type("application/json")
			resp.json(searchResult);
		})
		conn.release();
		return;
	})
});


app.use("libs", express.static(path.join(__dirname,"../bower_components")));
app.use(express.static(path.join(__dirname, "../client")));

const port = process.env.APP_PORT || 3000;

app.listen(port, function(){
    console.log("Application started at %s on port %d" ,
        new Date(), port);
});

